Source: pycoast
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Antonio Valentino <antonio.valentino@tiscali.it>
Section: python
Testsuite: autopkgtest-pkg-pybuild
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               dh-sequence-numpy3,
               dh-sequence-python3,
               dh-sequence-sphinxdoc,
               fonts-dejavu-core,
               pybuild-plugin-pyproject,
               python3-all,
               python3-aggdraw,
               python3-numpy,
               python3-pil,
               python3-pyproj,
               python3-pyresample,
               python3-pyshp,
               python3-pytest <!nocheck>,
               python3-pytest-lazy-fixtures <!nocheck>,
               python3-setuptools,
               python3-sphinx <!nodoc>,
               python3-sphinxcontrib.apidoc <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>,
               python3-versioneer
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/pycoast
Vcs-Git: https://salsa.debian.org/debian-gis-team/pycoast.git
Homepage: https://github.com/pytroll/pycoast
Rules-Requires-Root: no
Description: Draw coastlines, borders and rivers on images (for Python 3)
 Pycoast is a Python package to add coastlines, borders and rivers to
 raster images using data from the GSHHG (previously known as GSHHS)
 and WDBII datasets.
 .
 This package is part of the PyTroll toolset.

Package: python3-pycoast
Architecture: all
Depends: python3-aggdraw,
         python3-pil,
         python3-pyproj,
         python3-pyshp,
         ${python3:Depends},
         ${misc:Depends}
Suggests: fonts-dejavu-core,
          python-pycoast-doc
Description: ${source:Synopsis}
 ${source:Extended-Description}

Package: python-pycoast-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${sphinxdoc:Depends},
         ${misc:Depends}
Suggests: www-browser
Description: ${source:Synopsis} (documentation)
 ${source:Extended-Description}
 .
 This is the common documentation package.
